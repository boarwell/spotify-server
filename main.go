package main

import (
	"log"
	"net/http"

	"gitlab.com/gorilla_morimoto/spotify-server/playback"
	"gitlab.com/gorilla_morimoto/spotify-server/request"
	"gitlab.com/gorilla_morimoto/spotify-server/token"
	"golang.org/x/net/websocket"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		// DONE: token.Token を使ってSpotifyのAPIを叩く関数を作る
		body, err := request.Get()
		if err != nil {
			// TODO: token.Token が空だった場合は /auth/ にリダイレクト（わかるようにエラーを返す）
			http.Redirect(w, r, "/auth/", http.StatusSeeOther)
			return
		}
		w.Write(body)
	})

	// DONE: Websocketサーバーの実装
	// DONE: SpotifyのAPIからのレスポンスをWebsocketで送信する関数
	http.Handle("/playback/", websocket.Handler(playback.Serve))

	// /auth/ へのリクエストにcode=が含まれていなかったら
	// Spotifyのログインページにリダイレクトする
	//
	// code=が含まれていたら
	// そのcodeをtoken.Tokenにセットして / にリダイレクトする
	//
	// Spotifyのログインページからは /auth/ にリダイレクトする
	http.HandleFunc("/auth/", func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Query().Get("code") == "" {
			http.Redirect(w, r, token.AuthURL(), http.StatusSeeOther)
			return
		}

		err := token.FetchTokens(r.URL.Query().Get("code"))
		if err != nil {
			// TODO: /error/ のハンドリング
			http.Redirect(w, r, "/error/", http.StatusSeeOther)
			log.Println(err)
			return
		}
		http.Redirect(w, r, "/", http.StatusSeeOther)
	})

	http.ListenAndServe(":8080", nil)
}
