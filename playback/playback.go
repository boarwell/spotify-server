package playback

import (
	"log"
	"time"

	"gitlab.com/gorilla_morimoto/spotify-server/request"

	"golang.org/x/net/websocket"
)

func Serve(ws *websocket.Conn) {
	t := time.NewTicker(1 * time.Second)
	defer t.Stop()

	for {
		select {
		case <-t.C:
			b, e := request.Get()
			if e != nil {
				// TODO: access_tokenが有効期限切れだった場合に再発行する
				log.Println(e)
				break
			}
			ws.Write(b)
		}
	}
}
