package request

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/gorilla_morimoto/spotify-server/token"
)

func getToken() (token.Tokens, error) {
	t := token.Token
	if t == (token.Tokens{}) {
		err := requestError{
			uninitializedTokens,
			"access_token and refresh_token have not been initialized",
		}
		return token.Tokens{}, err
	}

	return t, nil
}

func buildRequest(t token.Tokens) (*http.Client, *http.Request) {
	endPoint := "https://api.spotify.com/v1/me/player"

	client := &http.Client{}
	req, _ := http.NewRequest("GET", endPoint, nil)
	req.Header.Add(
		"Authorization",
		fmt.Sprintf("Bearer %s", t.AccessToken),
	)

	return client, req
}

func Get() ([]byte, error) {
	t, err := getToken()
	if err != nil {
		return nil, err
	}

	client, req := buildRequest(t)
	resp, err := client.Do(req)
	if err != nil {
		e := requestError{
			networkError,
			"error when client.Do(req)",
		}
		return nil, e
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		e := requestError{
			invalidResponse,
			"invalid response from Spotify",
		}
		return nil, e
	}

	return body, nil
}
