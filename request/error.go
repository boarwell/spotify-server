package request

import "fmt"

type errorType int

const (
	// access_token and refresh_token have not been initialized
	uninitializedTokens errorType = iota
	// error when client.Do(req)
	networkError
	// invalid response from Spotify
	invalidResponse
)

var (
	m = map[errorType]string{
		uninitializedTokens: "UninitializedTokens",
		networkError:        "NetworkError",
		invalidResponse:     "InvalidResponseFromSpotify",
	}
)

type requestError struct {
	errorType
	message string
}

func (re requestError) Error() string {
	return fmt.Sprintf(
		"ErrorType: %s, %s", m[re.errorType], re.message,
	)
}
