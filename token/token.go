package token

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
)

// Tokens は codeを使って取得した
// access_token, refresh_tokenを保持する
type Tokens struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

const (
	responseType = "code"
	redirectURI  = "http://localhost:8080/auth/"
	grantType    = "authorization_code"
)

var (
	clientID     = os.Getenv("CLIENT_ID")
	clientSecret = os.Getenv("CLIENT_SECRET")
	// Token はaccess_tokenとrefresh_tokenを保持する
	Token = Tokens{}
)

// AuthURL はSpotifyのログイン画面のURLを返す
func AuthURL() string {
	base := "https://accounts.spotify.com/authorize/"

	v := url.Values{}
	v.Add("client_id", clientID)
	v.Add("response_type", responseType)
	v.Add("redirect_uri", redirectURI)
	v.Add("scope", "user-read-playback-state")

	// return v.Encode()
	return fmt.Sprintf("%s?%s", base, v.Encode())
}

// FetchTokens はcodeを引数にとって
// Tokenにaccess_tokenとrefresh_tokenをセットする
func FetchTokens(code string) error {
	base := "https://accounts.spotify.com/api/token"

	v := url.Values{
		"grant_type":    {grantType},
		"code":          {code},
		"redirect_uri":  {redirectURI},
		"client_id":     {clientID},
		"client_secret": {clientSecret},
	}

	resp, err := http.PostForm(base, v)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	err = json.Unmarshal(body, &Token)
	if err != nil {
		return err
	}

	log.Println(Token)
	return nil
}

func (t Tokens) String() string {
	return fmt.Sprintf(
		"{access_token: %s, refresh_token: %s}", t.AccessToken, t.RefreshToken,
	)
}
