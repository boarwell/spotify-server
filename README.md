## Reference

- https://developer.spotify.com/documentation/general/guides/authorization-guide/


## TODO

- [ ] Websocketクライアントの実装（JS）
- [ ] 各種エラー処理
- [ ] `refresh_token`を使って`access_token`を再発行する
